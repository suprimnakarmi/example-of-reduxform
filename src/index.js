import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./js/store/StoreIndex";
import App from "./js/components/App";
import "bootstrap/dist/css/bootstrap.min.css";
import $ from "jquery";
import Popper from "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";
render(
  <Provider store={store}>
    <App className="container" />
  </Provider>,
  document.getElementById("root")
);
