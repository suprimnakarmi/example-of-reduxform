import React from "react";
import ShowEmployee from "./ShowEmployee";
import { Route, BrowserRouter as Router } from "react-router-dom";
import AddEmployee from "./AddEmployee";
class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Route exact path="/" component={ShowEmployee} />
          <Route path="/employee/add" component={AddEmployee} />
        </div>
      </Router>
    );
  }
}

export default App;
