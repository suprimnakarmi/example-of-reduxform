import React from "react";
import rootReducer from "../reducers/Index";
import { connect } from "react-redux";
import axios from "axios";
import { showEmployees } from "../actions/EmployeeAction";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class ShowEmployee extends React.Component {
  componentDidMount() {
    this.getEmployees();
  }

  getEmployees() {
    // axios.get("/api/v1/employees").then(response => {
    // response.data;
    /**response chae redux store ma save hunu paryo */
    const { showEmployees } = this.props;
    // const showEmployees = this.props.showEmployees;
    showEmployees();
  }

  render() {
    const fetchEmployees = this.props.employees.map(employee => (
      <tr key={employee.id}>
        <td>{employee.name}</td>
        <td>{employee.position}</td>
        <td>{employee.details}</td>
        <td>{employee.VacationLeft}</td>

        <td>
          <button
            onClick={e => this.deleteEmployee(employee.id)}
            className="btn btn-outline-info"
          >
            Delete
          </button>
        </td>
      </tr>
    ));
    return (
      // <div className="container">
      <div className="shadow p-3 mb-5 bg-white rounded">
        {/* <BrowserRouter> */}
        <Link to="/employee/add" className="btn btn-info">
          New Employees
        </Link>
        {/* </BrowserRouter> */}
        <h6> List of Employees</h6>
        {/* <table className="table table-striped table-bordered"> */}
        <div>
          <table className="table table-dark">
            <thead>
              <tr>
                <th scope="col">name</th>
                <th scope="col">position</th>
                <th scope="col">details</th>
                <th scope="col">vacationleft</th>
                <th colSpan="2">Actions</th>
              </tr>
            </thead>

            <tbody>{fetchEmployees}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    employees: state.employees.items
  };
};

const mapDispatchToProps = {
  showEmployees
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowEmployee);
