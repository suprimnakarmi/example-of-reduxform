import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import { Field, reduxForm, SubmissionError } from "redux-form";
import { createEmployee } from "../actions/EmployeeAction";
import propTypes from "prop-types";
import { required, isEmail } from "../validations";
import { Input } from "./";

class AddEmployee extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      position: "",
      details: "",
      VacationList: "",
      errorMessage: false
    };

    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = async values => {
    let result = await axios
      .post("/api/v1/employees", {
        ...values,
        details: "",
        VacationLeft: 10
      })
      .catch(e => e.response);
    if (result.status !== 200) {
      const { name, position } = result.data;
      // throw new SubmissionError({ ...result.data.error });
      if (name && position) {
        throw new SubmissionError({ name: name[0], position: position[0] });
      }
      if (name) {
        throw new SubmissionError({ name: name[0] });
      }
      if (position) {
        throw new SubmissionError({ position: position[0] });
      }
    } else {
      this.props.createEmployee(result.data);
      this.props.history.push("/");
    }
  };

  render() {
    console.log(this.props);
    return (
      <div className="container">
        <div className="shadow p-3 mb-5 bg-white rounded">
          <form onSubmit={this.handleSubmit}>
            <Field
              component={Input}
              type="text"
              label="Name"
              name="name"
              validate={[required]}
              className="form-control col-4"
              {...this.props}
            />

            <Field
              component={Input}
              type="text"
              label="Email"
              name="position"
              validate={[required]}
              className="form-control col-4"
              {...this.props}
            />

            <button onClick={this.props.handleSubmit(this.handleSubmit)}>
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

createEmployee.propTypes = {
  createEmployee: propTypes.func.isRequired
};

const AddEmployeeForm = reduxForm({ form: "AddEmployee" })(AddEmployee);

const mapStateToProps = state => ({
  employees: state.employees.items
});

export default connect(
  mapStateToProps,
  { createEmployee }
)(AddEmployeeForm);
