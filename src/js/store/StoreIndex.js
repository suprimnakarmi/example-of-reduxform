import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/Index";
import thunk from "redux-thunk";
import logger from "redux-logger";

const middleWares = [thunk];
middleWares.push(logger);

const store = createStore(rootReducer, applyMiddleware(...middleWares));
export default store;
