import { SHOW_EMPLOYEES, NEW_EMPLOYEES } from "../constants/ActionTypes";

const initialState = {
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SHOW_EMPLOYEES:
      return {
        ...state,
        items: action.payload
      };
    // case DELETE_EMPLOYEE:
    //   return state.filter(employee => employee.id !== action.index);

    default:
      return state;
  }
}
