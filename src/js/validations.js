export function required(value) {
  return value ? undefined : "Required";
}

export function isEmail(email) {
  return /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)
    ? undefined
    : "Invalid Email";
}
