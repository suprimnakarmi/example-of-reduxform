import {
  ADD_EMPLOYEE,
  SHOW_EMPLOYEES,
  DELETE_EMPLOYEE
} from "../constants/ActionTypes";
import axios from "axios";

export const showEmployees = () => dispatch => {
  axios.get("/api/v1/employees").then(res =>
    dispatch({
      type: SHOW_EMPLOYEES,
      payload: res.data
    })
  );
};

export const createEmployee = employees => {
  return dispatch => {
    dispatch({ type: ADD_EMPLOYEE, payload: employees });
  };
};

export const deleteEmployee = id => dispatch => {
  axios.delete("/api/v1/employees/${id}").then(employee => dispatch({}));
};
